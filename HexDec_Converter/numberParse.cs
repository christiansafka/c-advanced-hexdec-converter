﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Globalization;

namespace HexDec_Converter
{
    class numberParse
    {
        public List<parsedOutput> getDec(string text)
        {
            List<parsedOutput> output = new List<parsedOutput>();
            string[] hexes = text.Split(' ', ';', ',', '\n');
            foreach (string hex in hexes)
            {
                if (!string.IsNullOrEmpty(hex))
                {
                    bool isValid;
                    bool isHex;
                    int number;
                    if (this.TryGetNumber(hex, out number, out isHex))
                    {
                        isValid = true;
                        output.Add(new parsedOutput(number, isHex, isValid));
                    }
                    else
                    {
                        isValid = false;
                        output.Add(new parsedOutput(int.MaxValue, isHex, isValid));  // MaxValue used as error value
                    }
                }
            }
            return output;
        }

        private bool TryGetNumber(string input, out int number, out bool isHex)
        {
            isHex = input.StartsWith("0x");
            input = (isHex) ? input.Substring(2) : input;
            return int.TryParse(input, (isHex) ? NumberStyles.HexNumber : NumberStyles.Integer, CultureInfo.CurrentCulture.NumberFormat, out number);
        }
    }
}
