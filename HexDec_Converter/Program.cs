﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexDec_Converter
{
    class Program
    {
        public static List<parsedOutput> parsedNumbers;
        public static string outputFilePath;
        public static string inputFilePath;

        static void Main(string[] args)
        {
            readArgs read = new readArgs();
            read.add("-i", input);
            read.add("-o", output);
            read.parse(args);
            if (parsedNumbers == null)
            {
                numberParse parse = new numberParse();
                parsedNumbers = parse.getDec(userinput());
            }

            if (outputFilePath != null && outputFilePath != inputFilePath)
            {
                outputFile(outputFilePath);
            }
            else
            {
                if (outputFilePath == inputFilePath)
                {
                    Console.WriteLine("Warning:  Output path is the same as input path");
                    Console.WriteLine("...proceeding without output path....");
                }
                display(parsedNumbers);
            }

            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey(true);

        }

        public static string userinput()
        {
            Console.WriteLine("Enter hexadecimals for conversion: ");
            string hexes = "";
            bool cont = true;
            while (cont == true)
            {
                hexes += Console.ReadLine();
                ConsoleKeyInfo info;
                do
                {
                    Console.WriteLine("\nContinue?  (y/n)");
                    info = Console.ReadKey(false);
                    if (info.Key == ConsoleKey.N)
                    {
                        cont = false;
                    }
                    else if (info.Key == ConsoleKey.Enter || info.Key == ConsoleKey.Y)
                    {
                        hexes += ";";
                        Console.WriteLine();
                    }
                }
                while (info.Key != ConsoleKey.Y && info.Key != ConsoleKey.N);
            }
            return hexes;
        }


        public static void display(List<parsedOutput> parsedNumbers)
        {
            Console.WriteLine("\nInput value             Output value");
            Console.Write(formatString());
        }

        public static void input(string iPath)
        {
            inputFilePath = iPath;
            
            if(Path.IsPathRooted(inputFilePath) == false)
            {
                inputFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, inputFilePath);    
            }
            string inputs = File.ReadAllText(inputFilePath);
            numberParse parse = new numberParse();
            parsedNumbers = parse.getDec(inputs);
        }

        public static void output(string oPath)
        {
            outputFilePath = oPath;
            if (Path.IsPathRooted(outputFilePath) == false)
            {
                outputFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outputFilePath);
            }
        }

        public static void outputFile(string path)
        {
            string relativePath = path;

            if (Path.IsPathRooted(path) == false)
            {
                relativePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);
            }
            StreamWriter sw = new StreamWriter(relativePath);
            sw.Write(formatString());
            sw.Close();
        }

        public static string formatString()
        {
            StringBuilder sb = new StringBuilder();

            int largestNumber = parsedNumbers.Max(o => o.Number).ToString().Length + 2;   // +2 for 0x prefix
            foreach (parsedOutput pn in parsedNumbers)
            {
                string input = pn.ToString();
                string output = (pn.IsHex) ? pn.Number.ToString() : "0x" + pn.Number.ToString("X");
                if (pn.IsValid == true)
                {
                    string line = string.Format("{0}{1}{2}", input, new string(' ', 24 - input.Length), output);
                    sb.AppendLine(line);
                }
                else
                {
                    sb.AppendLine(string.Format("Invalid {0} input", (pn.IsHex) ? "hex" : "decimal" )); 
                }
                
            }
            return sb.ToString();
        }

    }
}
