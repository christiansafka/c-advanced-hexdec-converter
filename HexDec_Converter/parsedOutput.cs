﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexDec_Converter
{
    class parsedOutput
    {
        public parsedOutput(int number, bool isHex, bool isValid)
        {
            this.Number = number;
            this.IsHex = isHex;
            this.IsValid = isValid;
        }

        public int Number { get; private set; }
        public bool IsHex { get; private set; }
        public bool IsValid { get; private set; }

        public override string ToString()
        {
            return (this.IsHex) ? ("0x" + this.Number.ToString("X")) : this.Number.ToString();
        }
    }
}
