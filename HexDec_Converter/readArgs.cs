﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HexDec_Converter
{
    class readArgs
    {
        public Dictionary<string, Action<string>> arguments = new Dictionary<string, Action<string>>();
        
        public void parse(string[] args)
        {
            if (args == null)
            {
                return;
            }
            int i = 0;
            foreach(string arg in args)
            {
                foreach(KeyValuePair<string, Action<string>> temp in arguments)
                {
                    if(arg == temp.Key)
                    {
                        if(args[i+1] != null)
                        {
                            temp.Value.Invoke(args[i+1]);
                        }
                    }
                }
                i++;
            }
        }

        public void add(string temp, Action<string> tempAction)
        {
            if (temp != null && tempAction != null)
            {
                arguments.Add(temp, tempAction);
            }
            else
            {
                throw new NullReferenceException();
            }
        }
    }
}
